# -*- coding: utf-8 -*-
import pika
from pyspark import SparkContext
import pickle
from datetime import datetime
import pyorient

client = pyorient.OrientDB("localhost", 2424)
session_id = client.connect("root", "0r13ntDB")
# OSM - open street map

if client.db_exists('REA'):
    database_info = client.db_open('REA', "root", "0r13ntDB")
    # getting positions of data clusters (neccessary for create/update)
    Real_estate_cluster_id = database_info.get_class_position('Real_estate')
    OSM_coords_cluster_id = database_info.get_class_position('OSM_coords')
else:
    client.db_create("REA", pyorient.DB_TYPE_GRAPH,
                     pyorient.STORAGE_TYPE_MEMORY)
    # creating two classes in DB with different data clusters
    Real_estate_cluster_id = client.command(
        "create class Real_estate extends V")[0]
    OSM_coords_cluster_id = client.command(
        "create class OSM_coords extends V")[0]

sc = SparkContext(appName="Filter")
connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))
channel = connection.channel()
channel.queue_declare(queue='task_queue', durable=True)


def parse_leftovers(leftovers):
    '''
    filter for each pair from nested dictionary of params from mlsn
    '''
    formated_key = leftovers[0].replace(
        ' ', '_').replace(',', '').replace('\'', '')
    formated_value = leftovers[1]
    if leftovers[1].lower() in ['да', 'есть']:
        formated_value = True
    if leftovers[1].lower() in ['нет', '—']:
        formated_value = False
    return formated_key, formated_value


def filter_func(pair):
    '''
    filter for each pair from object dictionary
    '''
    value = pair[1]
    if pair[0] in ['description', 'location']:
        value = pair[1].replace('\'', '').replace('\"', '')
    if isinstance(pair[1], datetime):
        value = pair[1].isoformat()
    return pair[0], value


def osm_filter(pair):
    '''
    cropping filter for lightweight objects (open street map)
    '''
    if pair[0] in ['longitude', 'location', 'latitude', 'ri_url', 'updated_at', 'created_at', 'ri_id']:
        return pair


def Real_estate_worker(vertex_class, cluster_id, data_dictionary):
    # worker for Real_estate objects that handles create and update actions
    search_query = client.query('select * from ' + vertex_class +
                                ' where ri_id=' + str(data_dictionary['ri_id']), 1, '*:0')
    result = {}
    if search_query:
        saved_dict = search_query[0].oRecordData
        last_update_time = datetime.strptime(
            data_dictionary['updated_at'], "%Y-%m-%dT%H:%M:%S")
        last_known_update_time = datetime.strptime(
            saved_dict['updated_at'], "%Y-%m-%dT%H:%M:%S")

        if last_update_time > last_known_update_time:
            # update or create history field for saving previous versions of
            # object data
            if 'history' not in saved_dict:
                data_dictionary['history'] = []
            else:
                data_dictionary['history'] = saved_dict['history']
                saved_dict.pop('history', None)

            data_dictionary['history'].append(
                {result['updated_at']: saved_dict}
            )
            client.record_update(
                cluster_id, search_query[0]._rid, data_dictionary)
            print 'Entry updated at ' + data_dictionary['updated_at']
    else:
        client.record_create(cluster_id, data_dictionary)
        print 'Entry created'


def OSM_coords_worker(vertex_class, cluster_id, data_dictionary):
    search_query = client.query('select * from ' + vertex_class +
                                ' where ri_id=' + str(data_dictionary['ri_id']), 1, '*:0')
    result = {}
    if search_query:
        saved_dict = search_query[0].oRecordData
        last_update_time = datetime.strptime(
            data_dictionary['updated_at'], "%Y-%m-%dT%H:%M:%S")
        last_known_update_time = datetime.strptime(
            saved_dict['updated_at'], "%Y-%m-%dT%H:%M:%S")

        if last_update_time > last_known_update_time:

            data_dictionary['history'].append(
                {result['updated_at']: saved_dict})
            client.record_update(
                cluster_id, search_query[0]._rid, data_dictionary)
            print 'OSM Entry updated at ' + data_dictionary['updated_at']
    else:
        client.record_create(cluster_id, data_dictionary)
        print 'OSM Entry created'


def callback(ch, method, properties, body):
    ch.basic_ack(delivery_tag=method.delivery_tag)
    # create Resilient Distributed Dataset from queue message
    rdd = sc.parallelize(pickle.loads(body).items())

    filtered_dictionary = dict(rdd.map(filter_func).collect())

    # creating another Resilient Distributed Dataset for nested dictionary and
    # applying filter to it
    leftovers_rdd = sc.parallelize(filtered_dictionary['leftovers'].items())
    leftovers_dict = dict(leftovers_rdd.map(parse_leftovers).collect())

    # crop this field from filtered_dictionary
    filtered_dictionary.pop('leftovers', None)

    # merging two processed dictionaries into one
    result = dict(filtered_dictionary.items() + leftovers_dict.items())

    # saving result with history
    Real_estate_worker('Real_estate', Real_estate_cluster_id, result)

    # applying osm filter and prepare dict to save
    OSM_dictionary = dict(rdd.filter(osm_filter).collect())

    # saving osm coords to another cluster
    OSM_coords_worker('OSM_coords', OSM_coords_cluster_id, OSM_dictionary)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback, queue='task_queue')
channel.start_consuming()
