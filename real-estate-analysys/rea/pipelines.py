# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pika
import pickle


class ReaPipeline(object):

    def process_item(self, item, spider):
        connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()
        channel.queue_declare(queue='task_queue', durable=True)

        channel.basic_publish(
            exchange='',
            routing_key='task_queue',
            body=pickle.dumps(dict(item)),
            properties=pika.BasicProperties(delivery_mode=2)  # make message persistent
        )
        connection.close()
        return item
