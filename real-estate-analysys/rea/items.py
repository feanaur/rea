# -*- coding: utf-8 -*-
import re
from datetime import datetime
from ast import literal_eval
from scrapy.item import Item, Field


def price_filter(value):
    if value:
        return int(value.replace(' ', ''))


def negotiate_price_filter(value):
    if not value:
        return True


def description_filter(value):
    return value.replace('\'', '')


def latitude_filter(value):
    tmp = re.search('initMapCard\(.*,.*\);', value)
    if tmp:
        coordinates = tmp.group(0).strip('initMapCard();')
        return float(coordinates.split(', ')[0])


def longitude_filter(value):
    tmp = re.search('initMapCard\(.*,.*\);', value)
    if tmp:
        coordinates = tmp.group(0).strip('initMapCard();')
        return float(coordinates.split(', ')[1])


def id_filter(value):
    return literal_eval(value)


def location_filter(value):
    location = ''
    for i in value:
        location += i.replace(u'\n', u'').replace(u'→',
                                                  u' ').replace(u'/', u'').strip()
    return location


def created_at_filter(value):
    return datetime.strptime(re.findall('\d+\.\d+.\d+', value)[0], "%d.%m.%Y")


def updated_at_filter(value):
    return datetime.strptime(re.findall('\d+\.\d+.\d+', value)[1], "%d.%m.%Y")


def leftovers_filter(bunch_of_rows):
    leftovers_dictionary = {}
    for row in bunch_of_rows:
        key_path = row.xpath('./th/text()')
        value_path = row.xpath('./td/text()')
        if any(value_path):
            leftovers_dictionary[key_path[0].extract().encode(
                'utf-8')] = value_path[0].extract().encode('utf-8')
        else:
            leftovers_dictionary[key_path[0].extract().encode('utf-8')] = ''
    return leftovers_dictionary


def unicode_decode(value):
    return value.encode('utf-8')


class ReaItem(Item):
    ri_url = Field()
    ri_id = Field()
    ri_type = Field()
    ri_subtype = Field()
    price = Field()
    negotiate_price = Field()
    description = Field()
    created_at = Field()
    updated_at = Field()
    location = Field()
    latitude = Field()
    longitude = Field()
    images = Field()
    contact = Field()
    leftovers = Field()
