# -*- coding: utf-8 -*-
import re
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.loader.processor import TakeFirst, Identity
from scrapy.contrib.loader import ItemLoader
from scrapy.selector import Selector
from scrapy.utils.markup import remove_tags
from rea.items import *

class ReaLoader(ItemLoader):
    default_output_processor = TakeFirst()
    ri_id_in = Compose(TakeFirst(),unicode_decode)
    price_in = Compose(Join(),price_filter)
    negotiate_price_in = Compose(negotiate_price_filter)
    location_in = Compose(location_filter,unicode_decode)
    created_at_in = Compose(TakeFirst(),created_at_filter)
    updated_at_in = Compose(TakeFirst(),updated_at_filter)
    ri_type_in  = Compose(TakeFirst(),description_filter,unicode_decode)
    ri_subtype_in = Compose(TakeFirst(),description_filter,unicode_decode)
    description_in = Compose(TakeFirst(),description_filter,unicode_decode)
    latitude_in = MapCompose(latitude_filter)
    longitude_in = MapCompose(longitude_filter)
    images_in = MapCompose(unicode_decode)
    images_out = Identity()
    leftovers_in = Compose(leftovers_filter)
    leftovers_out = Compose(Identity(),TakeFirst())

class ReaSpider(CrawlSpider):

    name = "mlsn"
    allowed_domains = ["omsk.mlsn.ru"]
    start_urls = [#Покупка недвижимости
                  "http://omsk.mlsn.ru/pokupka-nedvizhimosti/sale/?city_id=55&list_type=table&page=1&pageSize=100",
                  #Аренда недвижимости долгосрочная и краткосрочная
                  "http://omsk.mlsn.ru/arenda-nedvizhimosti/ls/?period=1&city_id=55&list_type=table&page=1&pageSize=100",
                  "http://omsk.mlsn.ru/arenda-nedvizhimosti/ls/?period=2&city_id=55&list_type=table&page=1&pageSize=100",
                  # Аренда коммерческой недвижимости
                  "http://omsk.mlsn.ru/arenda-kommercheskoj-nedvizhimosti/ls/?city_id=55&list_type=table&page=1&pageSize=100",
                  #Гаражи
                  "http://omsk.mlsn.ru/garazhi/ls/?city_id=55&list_type=table&page=1&pageSize=100",
                  #Дачи и земельные участки
                  "http://omsk.mlsn.ru/dachi-i-zemelnye-uchastki/ls/?city_id=55&list_type=table&page=1&pageSize=100",
                  ]
    rules =(
            #Покупка недвижимости
            Rule(SgmlLinkExtractor(allow=('.+.\/pokupka-nedvizhimosti\/sale\/\?city_id=55\&list_type=table\&page=\d+\&pageSize=100'), deny=('.*sort=.*'),unique=True), follow=True),
            Rule(SgmlLinkExtractor(allow=('(.*\/pokupka-nedvizhimosti\/.*-id\d*\/)'), unique=True, restrict_xpaths=('//div[@id="sale-search-grid"]')), callback='parse_item'),
            #Аренда недвижимости
            Rule(SgmlLinkExtractor(allow=('.+.\/arenda-nedvizhimosti\/ls\/\?period=[1,2]\&city_id=55\&list_type=table\&page=\d+\&pageSize=100'), deny=('.*sort=.*'),unique=True), follow=True),
            Rule(SgmlLinkExtractor(allow=('(.*\/arenda-nedvizhimosti\/.*-id\d*\/)'), unique=True, restrict_xpaths=('//div[@id="sale-search-grid"]')), callback='parse_item'),
            #Аренда коммерческой недвижимости
            Rule(SgmlLinkExtractor(allow=('.+.\/arenda-kommercheskoj-nedvizhimosti\/ls\/\?city_id=55\&list_type=table\&page=\d+\&pageSize=100'), deny=('.*sort=.*'),unique=True), follow=True),
            Rule(SgmlLinkExtractor(allow=('(.*\/arenda-kommercheskoj-nedvizhimosti\/.*-id\d*\/)'), unique=True, restrict_xpaths=('//div[@id="sale-search-grid"]','//div[@class="row-fluid card"]')), callback='parse_item'),
            #Гаражи
            Rule(SgmlLinkExtractor(allow=('.+.\/garazhi\/ls\/\?city_id=55\&list_type=table\&page=\d+\&pageSize=100'), deny=('.*sort=.*'),unique=True), follow=True),
            Rule(SgmlLinkExtractor(allow=('(.*\/garazhi\/.*-id\d*\/)'), unique=True, restrict_xpaths=('//div[@id="sale-search-grid"]')), callback='parse_item'),
            #Дачи и земельные участки
            Rule(SgmlLinkExtractor(allow=('.+.\/dachi-i-zemelnye-uchastki\/ls\/\?city_id=55\&list_type=table\&page=\d+\&pageSize=100'), deny=('.*sort=.*'),unique=True), follow=True),
            Rule(SgmlLinkExtractor(allow=('.*\/dachi-i-zemelnye-uchastki\/.*-id\d*\/'), unique=True, restrict_xpaths=('//div[@id="sale-search-grid"]')), callback='parse_item')
            )

    def parse_item(self, response):
        hxs = Selector(response)
        rea_loader = ReaLoader(ReaItem(), hxs)
        rea_loader.add_value('ri_url',response.url)
        
        header = '//span[contains(@class,"muted nowrap")]'
        rea_loader.add_xpath('ri_id',header+'/span[@itemprop="productID"]/text()')
        rea_loader.add_xpath('created_at',header)
        rea_loader.add_xpath('updated_at',header)   

        rea_loader.add_xpath('ri_type','//ul[contains(@class,"breadcrumb")]/li[contains(@id,"breadcrumb-1")]/a/span/text()')
        rea_loader.add_xpath('ri_subtype','//ul[contains(@class,"breadcrumb")]/li[contains(@id,"breadcrumb-2")]/a/span/text()')
        rea_loader.add_xpath('price', "//div[contains(@class,'block-price')]//span[@itemprop='price']/text()")
        rea_loader.add_xpath('negotiate_price', "//div[contains(@class,'block-price')]//span[@itemprop='price']/text()")
        rea_loader.add_xpath('location','//div[contains(@class, "reference")]/text()')        
        rea_loader.add_xpath('description',"//p[@itemprop='description']/text()")
        rea_loader.add_xpath('longitude','//script/text()')
        rea_loader.add_xpath('latitude','//script/text()')
        rea_loader.add_xpath('images',"//div[@class='description']/a/img/@src")
        rea_loader.add_value('leftovers',hxs.xpath('//table[contains(@class,"card-detail-view")]/tr'))
        return rea_loader.load_item()
