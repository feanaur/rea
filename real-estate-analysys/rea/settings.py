# -*- coding: utf-8 -*-

# Scrapy settings for rea project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'rea'

SPIDER_MODULES = ['rea.spiders']
ITEM_PIPELINES = {
    'rea.pipelines.ReaPipeline': 300
}
NEWSPIDER_MODULE = 'rea.spiders'
DOWNLOAD_TIMEOUT = 15
DOWNLOAD_DELAY = 0.25
# LOG_LEVEL='ERROR'
# LOG_STDOUT = True
# LOG_FILE = './scrapy_output.txt'
# REDIRECT_ENABLED = False
# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0'
