# -*- coding: utf-8 -*-
"""
    Simple sockjs-tornado chat application. By default will listen on port 8080.
"""
import tornado.ioloop
import tornado.web
import pyorient
import sockjs.tornado
import json
from datetime import datetime


class IndexHandler(tornado.web.RequestHandler):
    """Regular HTTP handler to serve the chatroom page"""
    def get(self):
        self.render('index.html')


class ChatConnection(sockjs.tornado.SockJSConnection):
    """Chat connection implementation"""
    # Class level variable
    participants = set()

    def on_open(self, info):
        # Send that someone joined
        self.broadcast(self.participants, "Someone joined.")

        # Add client to the clients list
        self.participants.add(self)

    def on_message(self, message):
        # Broadcast message
        if client.db_exists('REA'):
            database_info = client.db_open('REA', "root", "0r13ntDB")
            Real_estate_cluster_id = database_info.get_class_position('Real_estate')
            data = client.record_load(str(Real_estate_cluster_id) + ':' + str(message)).oRecordData
            data = client.query("SELECT * FROM OSM_coords", -1)
            for dot in data:
                sample = dot.oRecordData
                sample = {k: v for (k, v) in sample.items() if not isinstance(v, datetime)}
                self.send(json.dumps(sample))

    def on_close(self):
        # Remove client from the clients list and broadcast leave message
        self.participants.remove(self)

        self.broadcast(self.participants, "Someone left.")


if __name__ == "__main__":
    import logging
    logging.getLogger().setLevel(logging.DEBUG)

    # 1. Create chat router
    ChatRouter = sockjs.tornado.SockJSRouter(ChatConnection, '/chat')

    # 2. Create Tornado application
    app = tornado.web.Application(
        [(r"/", IndexHandler)] + ChatRouter.urls
    )
    client = pyorient.OrientDB("localhost", 2424)

    session_id = client.connect("root", "0r13ntDB")
    # 3. Make Tornado app listen on port 8080
    app.listen(8080)

    # 4. Start IOLoop
    tornado.ioloop.IOLoop.instance().start()
